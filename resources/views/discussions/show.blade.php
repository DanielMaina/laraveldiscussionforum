@extends('layouts.app')

@section('content')
<div class="card">
@include('partials.discussion-header')
   <div class="card-body">
   <div class="text-center">
   <strong>
   {{ $discussion->title }}
   </strong>
   </div>
  <hr>
  {!! $discussion->content !!}
  @if($discussion->bestReply)
   <div class="card mt-3">
   <div class="card-header bg-success">
   Best Reply by <strong> {{$discussion->bestReply->onwer->name}} </strong>
   </div>
   <div class="card-body">
   {!! $discussion->bestReply->content !!}
   </div>
   </div>
  @endif
</div>
</div>
@foreach($discussion->replies()->paginate(3) as $reply)

<div class="card my-2">
<div class="card-header">
<div class="d-flex justify-content-between">
<div>
{{$reply->onwer->name}}
</div>
<div>
@auth
@if(auth()->user()->id === $discussion->user_id)
<form action="{{route('discussions.best-reply',['discussion'=>$discussion->slug,'reply'=>$reply->id]) }}" method="POST">
@csrf
<button type="submit" class="btn btn-outline-info btn-sm">mark as best reply</button>
</form>
@endif
@endauth
</div>
</div>
</div>
<div class="card-body">
{!! $reply->content !!}
</div>
</div>

@endforeach
{{$discussion->replies()->paginate(3)->links()}}
<div class="card my-2">
<div class="card-header">
Add a reply
</div>
<div class="card-body">
@auth
<form action="{{route('replies.store', $discussion->slug)}}" method="POST">
@csrf
 <input type="hidden" name="content" id="content">
 <trix-editor input="content"></trix-editor>
 <button class="btn btn-success btn-sm my-2" type="submit">Add Reply</button>
</form>
@else
<a href="{{route('login')}}" class="btn btn-success btn-sm">Sign In To Reply</a>
@endauth
</div>
</div>
@endsection

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.0.0/trix.css">
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.0.0/trix.js"></script>
@endsection