<?php

namespace LaravelForum;

use LaravelForum\Notifications\ReplyMarkAsBestReply;


class Discussion extends Model
{
    //create a relationship
    public function author(){
        return $this->belongsTo(User::class,'user_id');
    }
   //Get Replies for a specific discussion
   public function replies(){
       return $this->hasMany(Reply::class);
   }

    //change Route model biding to use slug

    public function getRouteKeyName(){
        return 'slug';
    }
/**
 * 
 * @method Check if discussion has best reply 
 * 
 */
 public function bestReply(){
     return $this->belongsTo(Reply::class,'reply_id');
 }
 /**
  * scope method to handle filter by channel
  */

 public function scopeFilterByChannels($builder){
     if(request()->query('channel')){
        //filter
        $channel = Channel::where('slug',request()->query('channel'))->first();
        if($channel){
            return $builder->where('channel_id',$channel->id);
        }
        return $builder;
    
   }
   return $builder;
 }


    /**
     * 
     * @method decogest control 
     */
    public function markAsBestReply(Reply $reply){
        $this->update([
            'reply_id'=>$reply->id
        ]);
        if($reply->onwer->id ===$this->author->id){
            return;
        }
        $reply->onwer->notify(new ReplyMarkAsBestReply($reply->discussion));
    
    }
}
