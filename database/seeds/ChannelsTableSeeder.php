<?php

use Illuminate\Database\Seeder;
use LaravelForum\Channel;

class ChannelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Channel::create([
            'name' => 'Laravel 5.8',
            'slug'=>str_slug("Laravel 5.8")
        ]);
        Channel::create([
            'name' => 'React js 16',
            'slug'=>str_slug("React 16")
        ]);
        Channel::create([
            'name' => 'Vue js 4.4',
            'slug'=>str_slug("Vue js 4.4")
        ]);
        Channel::create([
            'name' => 'Node js 14.0',
            'slug'=>str_slug("Node js 14.0")
        ]);
    }
}
